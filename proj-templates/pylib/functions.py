

def foo(bar: float) -> float:
    """
    Return 0.0 for any input.
    :param bar: input number
    :return: 0.0
    """
    return 0.0
