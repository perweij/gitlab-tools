

import setuptools

setuptools.setup(
    name='{{NAME}}',
    packages=setuptools.find_packages(include=['functions']),
    version='0.1.0',
    description='Python library',
    author='{{AUTHOR}}',
    license='GPLv3',
)
