get_standard_readmes:
	mkdir -p standard_readmes
	curl -s 'https://gitlab.com/gitlab-org/gitlab/-/raw/master/app/views/projects/readme_templates/default.md.tt' > standard_readmes/gitlab.md


mk_readme:
	pod2markdown < list-gitlab-stuff.pl | sed 's|\\`|`|g'  > README.md


build:
	podman build -t list-gitlab-stuff .
