#!/usr/bin/env perl
###############################################################################
#
# File:           list-gitlab-stuff
#
# Author:         Per Weijnitz
# E-Mail:         per.weijnitz@arbetsformedlingen.se
# Org:            arbetsformedlingen.se
# License:        GPLv3
#


=head1 NAME

list-gitlab-stuff - a script to dump data from the Gitlab API


=head1 SYNOPSIS

list-gitlab-stuff ( -g ) secrets-file ( id-of-Gitlab-group)

The default mode is to dump data about projects.

 Optional flags:
  -g                  List groups instead of projects

 Optional arguments:
  id-of-Gitlab-group  Supply the top Gitlab group's ID

=head2 RUNNING FROM PODMAN OR DOCKER

After you have build your image with `make build`, you can run it like this:
```
  podman run -v $PWD:$PWD:Z --rm -i list-gitlab-stuff $PWD/secrets.sh > result.jsonl
```

=head1 DESCRIPTION

This program recursively extracts data from a top group in
Gitlab. Most data is readily available with API queries, but some
extra fields are added to these responses.

=head2 Results

The output is in JSONL format.

=head1 INSTALLATION

Either build a podman image with `make build` (default image name
`list-gitlab-stuff`) or setup your environment with the requirements
listed in Dockerfile.

=head1 CONFIGURATION

You need to prepare a secrets file with two fields:

 GL_DOMAIN="https://gitlab.com"
 GL_TOKEN="<your token>"

Use a Gitlab token with sufficiently high permissions, and make sure you keep it protected
and at least to C<chmod go= secretsfile> to prohibit other users from reading it.

=head1 UTILITIES

The main program produces a JSONL-file which can be fed to the utility script `stats.sh`:
```
$ ./list-gitlab-stuff.pl secrets.sh > results.jsonl
$ bash stats.sh < results.jsonl

Projects with Aardvark:                  130/436
Projects with Gitlab CI:                 57/436
Projects with codeowners:                8/436
Projects with license:                   127/436
Projects with JobTech CI:                36/436
Projects with readme:                    354/436
Public projects with Aardvark:           120/436
Public projects with Gitlab CI:          51/436
Public projects with codeowners:         7/436
Public projects with license:            119/436
Public projects with JobTech CI:         32/436
Public projects with readme:             308/436
Private projects with Aardvark:          10/436
Private projects with Gitlab CI:         6/436
Private projects with codeowners:        1/436
Private projects with license:           8/436
Private projects with JobTech CI:        4/436
Private projects with readme:            46/436
```

=head1 AUTHOR

Written by Per Weijnitz.

=head1 CONTRIBUTION AND REPORTING BUGS

Yes please - check the Project's Gitlab page.

=head1 LICENSE
GPLv3


=cut
use strict;
use warnings;
use autodie qw(:all);
use v5.10;

use LWP::UserAgent;
use URI;
use JSON::PP;
use File::Temp;
use File::Basename;
use Pod::Usage qw(pod2usage);



#### global init (FIXME: add proper optparsing)
my $list_type  = 'projects';
if ($ARGV[0] eq '-h') {
    invocation_exit(0);
}
if ($ARGV[0] eq '-g') {
  $list_type  = 'groups'; shift;
}
my $settings   = parse_settings(shift(@ARGV));
my $topgroup   = shift // 7094694;  # 7094694 = AF top group, 15917992 = per-test
my $baseurl    = $settings->{'GL_DOMAIN'}."/api/v4/";
my $baseparams = { 'private_token' => $settings->{'GL_TOKEN'} };
my $basedir    = dirname(__FILE__);



#### main
if ($list_type eq 'projects') {
  main_list_projects();
} else {
  main_list_groups();
}



#### functions


sub invocation_exit {
    my $exitstatus = shift;
    my $message    = shift;
    pod2usage(-verbose => 0, -exitval => 0);
    if($message) {
	warn($message);
    }
    exit $exitstatus;
}



sub main_list_projects {
  my $groupiter = get_all_groups($topgroup);
  while (my $group = $groupiter->()) {
    my $projiter = get_all_projs_in_group($group->{'id'});
    while (my $proj = $projiter->()) {
      say STDERR $proj->{'namespace'}->{'full_path'}."/".$proj->{'path'}."...";
      my %params = %{$baseparams};
      $params{'license'} = 1;
      $params{'statistics'} = 1; # FIXME: check what is added by this
      my $pobj = getjson($baseurl."projects/".$proj->{'id'}, \%params);
      #$pobj->{'name'}          = $proj->{'path_with_namespace'};
      $pobj->{'access_tokens'} = getjson($baseurl."projects/".$proj->{'id'}."/access_tokens");
      $pobj->{'deploy_tokens'} = getjson($baseurl."projects/".$proj->{'id'}."/deploy_tokens");
      $pobj->{'deploy_keys'}   = getjson($baseurl."projects/".$proj->{'id'}."/deploy_keys");
      $pobj->{'webhooks'}      = getjson($baseurl."projects/".$proj->{'id'}."/hooks");

      %params = %{$baseparams};
      $params{'recursive'} = 'true';
      my $files         = getjson($baseurl."projects/".$proj->{'id'}."/repository/tree", \%params);
      if (ref $files eq 'ARRAY') {
	check_has_gitlabci($pobj, $files);
	check_jobtechci($pobj, $files, $proj->{'id'});
	$pobj->{'has_codeowners'} = ( grep { $_->{'path'} eq "CODEOWNERS"
					       || $_->{'path'} eq ".gitlab/CODEOWNERS"
					       || $_->{'path'} eq "docs/CODEOWNERS" } @{$files} ) ? 1 : 0;
	score_readme($pobj, $files, $proj->{'id'});
	# FIXME: go through that checklist of a good open source repo
	# FIXME: publish in MPR
      } else {
	say STDERR " - repo is empty\n";
      }
      say encode_json($pobj);
    }
  }
}


sub score_readme {
    my ($pobj, $files, $projid) = @_;
    my $score = 0;

    if($pobj->{'readme_url'}) {

	$pobj->{'readme_url'} =~ m<^.*/([^/]+)$>;
	my $readme_basename = $1;
	my $url = $baseurl."projects/".$projid."/repository/files/$readme_basename/raw";
	my $readmetext = get($url);
	if($readmetext) {
	    my $tmp = File::Temp->new();
	    open(my $fh, '|-', "perl $basedir/score_readme.pl > $tmp")
		or die "Couldn't open a pipe: $!";
	    print $fh $readmetext;
	    close($fh);
	    open($fh, "<", $tmp) || die "Couldn't open $tmp: $!";
	    chomp($score = <$fh>);
	    close($fh);
	}
    }
    $pobj->{'readme_score'} = $score;
}


sub main_list_groups {
  my $groupiter = get_all_groups($topgroup);
  while (my $group = $groupiter->()) {
    my $gobj = getjson($baseurl."groups/".$group->{'id'});
    $gobj->{'access_tokens'} = getjson($baseurl."groups/".$group->{'id'}."/hooks");
    $gobj->{'deploy_tokens'}  = getjson($baseurl."groups/".$group->{'id'}."/deploy_tokens");
    say encode_json($gobj);
  }
}



sub check_has_gitlabci {
  my ($pobj, $files) = @_;
  my $has_gitlabci = 0;
  if (grep { $_->{'name'} =~ /^\.gitlab-ci\.yml$/  } @{$files}) {
    $has_gitlabci = 1;
  }
  $pobj->{'has_gitlabci'} = $has_gitlabci;
}


sub check_jobtechci {
  my ($pobj, $files, $projid) = @_;
  my $jobtechci = "n/a";

  if ($pobj->{'has_gitlabci'}) {
    my $gitlabci = get($baseurl."projects/".$projid."/repository/files/%2Egitlab%2Dci%2Eyml/raw");
    if ($gitlabci =~ /^[^#]*remote: .*jobtech-ci\/-\/raw\/(.*?)\/jobtech-ci.yml/m) {
      $jobtechci = $1;
    }
  }
  $pobj->{'jobtechci'} = $jobtechci;
}



sub getjson {
  my $res = get(@_);

  return patch_decoded_json(decode_json($res));
}



sub get {
  my $uri        = shift;
  my $formparams = shift // $baseparams;
  my $url = URI->new($uri);

  $url->query_form( %{$formparams} );
  my $res = LWP::UserAgent->new->get( $url, cookie_jar => {});
  unless($res->is_success) {
    warn("FAILED: $url");
  }
  return $res->decoded_content;
}



sub get_all_groups {
  my $topgroup = shift;
  my $cache = [];
  my $page = 1;
  my $first = 1;
  return sub {
    unless (@{$cache}) {
      if ($first) {
	$first = 0;
	$cache = [ getjson($baseurl."groups/".$topgroup) ];
      }
      my %params               = %{$baseparams};
      $params{'all_available'} = 'true';
      $params{'per_page'}      = 100;
      $params{page}            = $page;
      push(@{$cache}, @{getjson($baseurl."groups/".$topgroup."/descendant_groups", \%params)});
      return unless(@{$cache});
      $page++;
    }
    return shift(@{$cache});
  };
}



sub get_all_projs_in_group {
  my $group = shift;
  my $cache = [];
  my $page = 1;
  return sub {
    unless (@{$cache}) {
      my %params               = %{$baseparams};
      $params{'per_page'}      = 100;
      $params{page}            = $page;
      $cache = getjson($baseurl."/groups/".$group."/projects", \%params);
      return unless(@{$cache});
      $page++;
    }
    return pop(@{$cache});
  };
}



sub parse_settings {
  my $file = shift;
  my %settings = ();
  open(my $in, $file) || invocation_exit(1, "cannot open $file");
  while (my $line = <$in>) {
    if ($line =~ /^([^=]*)=["']?(.*?)["']?$/) {
      $settings{$1} = $2;
    }
  }
  close($in);

  return \%settings;
}



# stupid patch for decode_json (it doesn't decode Booleans reasonably)
sub patch_decoded_json {
  my $node           =   shift @_;

  if (ref $node eq 'ARRAY') {
    map { patch_decoded_json($_) } @{$node};
  } else {
    foreach my $key ( keys %{$node} ) {
      if (ref $node->{$key} eq 'JSON::PP::Boolean') {
	$node->{$key} = $node->{$key} ? 1 : 0;
      }
      if (ref $node->{$key} eq 'HASH') {
	patch_decoded_json($node->{$key});
      }
    }
  }
  return $node;
}
