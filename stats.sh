#!/bin/bash

report=$(mktemp)
trap "rm -f $report" EXIT

# read jsonl from stdin
cat - > "$report"


FILTER_PUBLIC='select(.visibility == "public")'
FILTER_PRIVATE='select(.visibility != "public")'
FILTER_HAS_AARDVARK='select(.webhooks[] | .url | contains("build") )'
FILTER_HAS_GITLABCI='select(.has_gitlabci == 1)'
FILTER_HAS_CODEOWNERS='select(.has_codeowners == 1)'
FILTER_HAS_LICENSE='select(.license)'
FILTER_HAS_JOBTECHCI='select(.jobtechci != "n/a")'
FILTER_HAS_README='select(.readme_url)'



function report() {
    local title="${1:-not-set}"; shift
    local NR_PROJS="${1:-0}"; shift
    local filter=$(IFS='|' ; echo "$*")

    local NR_MATCHES=$(jq -rc "$filter"' | .name' < "$report" | sort | uniq | wc -l)

    printf "%-40s %d/%d\n" "$title:" "$NR_MATCHES" "$NR_PROJS"
}


NR_PROJS=$(jq -c '.' < "$report" |wc -l)
NR_PROJS_PRIV=$(jq -c "$FILTER_PRIVATE" < "$report" |wc -l)
NR_PROJS_PUBL=$(jq -c "$FILTER_PUBLIC" < "$report" |wc -l)


report "Projects with Aardvark"           "$NR_PROJS" "$FILTER_HAS_AARDVARK"
report "Projects with Gitlab CI"          "$NR_PROJS" "$FILTER_HAS_GITLABCI"
report "Projects with codeowners"         "$NR_PROJS" "$FILTER_HAS_CODEOWNERS"
report "Projects with license"            "$NR_PROJS" "$FILTER_HAS_LICENSE"
report "Projects with JobTech CI"         "$NR_PROJS" "$FILTER_HAS_JOBTECHCI"
report "Projects with readme"             "$NR_PROJS" "$FILTER_HAS_README"

report "Public projects with Aardvark"    "$NR_PROJS_PUBL" "$FILTER_PUBLIC" "$FILTER_HAS_AARDVARK"
report "Public projects with Gitlab CI"   "$NR_PROJS_PUBL" "$FILTER_PUBLIC" "$FILTER_HAS_GITLABCI"
report "Public projects with codeowners"  "$NR_PROJS_PUBL" "$FILTER_PUBLIC" "$FILTER_HAS_CODEOWNERS"
report "Public projects with license"     "$NR_PROJS_PUBL" "$FILTER_PUBLIC" "$FILTER_HAS_LICENSE"
report "Public projects with JobTech CI"  "$NR_PROJS_PUBL" "$FILTER_PUBLIC" "$FILTER_HAS_JOBTECHCI"
report "Public projects with readme"      "$NR_PROJS_PUBL" "$FILTER_PUBLIC" "$FILTER_HAS_README"

report "Private projects with Aardvark"   "$NR_PROJS_PRIV" "$FILTER_PRIVATE" "$FILTER_HAS_AARDVARK"
report "Private projects with Gitlab CI"  "$NR_PROJS_PRIV" "$FILTER_PRIVATE" "$FILTER_HAS_GITLABCI"
report "Private projects with codeowners" "$NR_PROJS_PRIV" "$FILTER_PRIVATE" "$FILTER_HAS_CODEOWNERS"
report "Private projects with license"    "$NR_PROJS_PRIV" "$FILTER_PRIVATE" "$FILTER_HAS_LICENSE"
report "Private projects with JobTech CI" "$NR_PROJS_PRIV" "$FILTER_PRIVATE" "$FILTER_HAS_JOBTECHCI"
report "Private projects with readme"     "$NR_PROJS_PRIV" "$FILTER_PRIVATE" "$FILTER_HAS_README"
