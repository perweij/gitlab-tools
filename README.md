# NAME

list-gitlab-stuff - a script to dump data from the Gitlab API

# SYNOPSIS

list-gitlab-stuff ( -g ) secrets-file ( id-of-Gitlab-group)

The default mode is to dump data about projects.

    Optional flags:
     -g                  List groups instead of projects

    Optional arguments:
     id-of-Gitlab-group  Supply the top Gitlab group's ID

## RUNNING FROM PODMAN OR DOCKER

After you have build your image with `make build`, you can run it like this:
```
  podman run -v $PWD:$PWD:Z --rm -i list-gitlab-stuff $PWD/secrets.sh > result.jsonl
```

# DESCRIPTION

This program recursively extracts data from a top group in
Gitlab. Most data is readily available with API queries, but some
extra fields are added to these responses.

## Results

The output is in JSONL format.

# INSTALLATION

Either build a podman image with `make build` (default image name
`list-gitlab-stuff`) or setup your environment with the requirements
listed in Dockerfile.

# CONFIGURATION

You need to prepare a secrets file with two fields:

    GL_DOMAIN="https://gitlab.com"
    GL_TOKEN="<your token>"

Use a Gitlab token with sufficiently high permissions, and make sure you keep it protected
and at least to `chmod go= secretsfile` to prohibit other users from reading it.

# UTILITIES

The main program produces a JSONL-file which can be fed to the utility script `stats.sh`:
```
$ ./list-gitlab-stuff.pl secrets.sh > results.jsonl
$ bash stats.sh < results.jsonl

Projects with Aardvark:                  130/436
Projects with Gitlab CI:                 57/436
Projects with codeowners:                8/436
Projects with license:                   127/436
Projects with JobTech CI:                36/436
Projects with readme:                    354/436
Public projects with Aardvark:           120/436
Public projects with Gitlab CI:          51/436
Public projects with codeowners:         7/436
Public projects with license:            119/436
Public projects with JobTech CI:         32/436
Public projects with readme:             308/436
Private projects with Aardvark:          10/436
Private projects with Gitlab CI:         6/436
Private projects with codeowners:        1/436
Private projects with license:           8/436
Private projects with JobTech CI:        4/436
Private projects with readme:            46/436
```

# AUTHOR

Written by Per Weijnitz.

# CONTRIBUTION AND REPORTING BUGS

Yes please - check the Project's Gitlab page.

# LICENSE
GPLv3
