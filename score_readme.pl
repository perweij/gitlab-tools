#!/usr/bin/env perl
use strict;
use warnings;
use v5.10;
use String::Similarity;

use File::Basename;


my $readmetxt        = do { local $/; <STDIN> };
my $score            = 0.0;

my $basedir          = dirname(__FILE__);
my @standard_readmes = <$basedir/standard_readmes/*>;

# readmes with a total string length under this threshold
# are automatically worst score
my $length_threshold = 300;


if(length($readmetxt) >= $length_threshold) {
    my $doc;
    foreach my $file (@standard_readmes) {
	say STDERR "comparing to $file...";
	open(my $fh, '<', $file) || die();
	my $stdreadme = do { local $/; <$fh> };
	close($fh);
	my $sim = similarity($readmetxt, $stdreadme);
	if($sim == ($score, $sim)[$score < $sim]) {   # max(x,y)
	    $doc = $file;
	    $score = ($score, $sim)[$score < $sim];   # max(x,y)
	}
    }
    if($doc) {
	# score is the inverse of the similarity
	$score = 1.0 - $score;

	say STDERR "score against $doc: $score";
    }
} else {
    say STDERR "input readme is shorter than the threshold of $length_threshold";
}

say $score;
