FROM docker-mirror.jobtechdev.se/debian:stable-20230814-slim AS base

RUN apt-get -y update \
    && apt-get -y install --no-install-recommends libjson-perl \
       libipc-system-simple-perl libwww-perl libstring-similarity-perl jq

WORKDIR /app

COPY list-gitlab-stuff.pl score_readme.pl stats.sh /app/



###############################################################################
FROM base AS dataloaders

RUN apt-get -y install --no-install-recommends make curl

COPY Makefile .

RUN make



###############################################################################
FROM base AS test

COPY --from=dataloaders /app/standard_readmes /app/standard_readmes

RUN ./list-gitlab-stuff.pl -h >/dev/null &&\
    if [ $(./score_readme.pl < /app/standard_readmes/gitlab.md) != "0" ]; then echo "bad score in test" >&2; exit 1; fi &&\
    touch /.tests-successful



###############################################################################
FROM base AS final

COPY --from=test /.tests-successful /

RUN apt-get -y autoremove --purge \
    && apt-get -y clean \
    && rm -rf /var/lib/apt/lists/*

COPY --from=dataloaders /app/standard_readmes /app/standard_readmes

ENTRYPOINT [ "/app/list-gitlab-stuff.pl" ]
